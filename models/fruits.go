package models

type (
	Fruits struct {
		ID       int    `json:"id"`
		Name     string `jsno:"name"`
		Color    string `json:"color"`
		Location string `json:"location"`
		Rating   int    `json:"rating"`
	}
)
