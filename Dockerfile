FROM golang:latest

LABEL maintainer="Shariful Islam <si.nayon@gmail.com>"
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o server .
EXPOSE 1323
CMD ["./server"]