package handlers

import (
	"api/models"
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

var (
	fruits = map[int]*models.Fruits{}
	seq    = 1
)

func CreateFruits(c echo.Context) error {
	u := &models.Fruits{
		ID: seq,
	}
	fmt.Println(u)
	if err := c.Bind(u); err != nil {
		return err
	}
	fmt.Println(u)
	fruits[u.ID] = u
	seq++
	return c.JSON(http.StatusCreated, u)
}

func GetFruits(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	return c.JSON(http.StatusOK, fruits[id])
}

func GetAll(c echo.Context) error {
	return c.JSON(http.StatusOK, fruits)
}

func UpdateFruits(c echo.Context) error {
	u := new(models.Fruits)
	if err := c.Bind(u); err != nil {
		return err
	}
	id, _ := strconv.Atoi(c.Param("id"))
	fruits[id].Name = u.Name
	fruits[id].Color = u.Color
	fruits[id].Location = u.Location
	fruits[id].Rating = u.Rating

	return c.JSON(http.StatusOK, fruits[id])
}

func DeleteFruits(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	delete(fruits, id)
	return c.NoContent(http.StatusNoContent)
}
