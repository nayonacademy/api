package main

import (
	"api/handlers"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()

	// middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// Routes

	e.POST("/fruit", handlers.CreateFruits)
	e.GET("/fruit/:id", handlers.GetFruits)
	e.GET("/fruit", handlers.GetAll)
	e.PUT("/fruit/:id", handlers.UpdateFruits)
	e.DELETE("/fruit/:id", handlers.DeleteFruits)
	//  start server
	e.Logger.Fatal(e.Start(":1323"))
}
